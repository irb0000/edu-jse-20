package com.nlmk.sychikov.jse.shapes;

public class Square extends Shape{
    private final double squareSide;

    public Square(double squareSide) {
        this.squareSide = squareSide;
    }

    @Override
    public double getArea() {
        return squareSide * squareSide;
    }
}
