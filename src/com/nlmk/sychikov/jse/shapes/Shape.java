package com.nlmk.sychikov.jse.shapes;

public abstract class Shape {
    public abstract double getArea();

}
