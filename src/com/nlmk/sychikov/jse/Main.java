package com.nlmk.sychikov.jse;

import com.nlmk.sychikov.jse.shapes.Circle;
import com.nlmk.sychikov.jse.shapes.Rectangle;
import com.nlmk.sychikov.jse.shapes.Shape;
import com.nlmk.sychikov.jse.shapes.Square;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Main {
    static <T extends Shape> void addShapes(final Collection<T> shapes, final T... shape) {
        shapes.addAll(Arrays.asList(shape));
    }

    static double getSumArea(final Collection<? extends Shape> shapes) {
        double result = 0.0;
        for (final Shape shape : shapes) {
            result += shape.getArea();
        }
        return result;
    }

    public static void main(String[] args) {
        final Collection<Circle> circles = new ArrayList<>();
        final Collection<Rectangle> rectangles = new ArrayList<>();
        final Collection<Square> squares = new ArrayList<>();
        final Collection<Shape> shapes = new ArrayList<>();
        addShapes(circles, new Circle(9.9), new Circle(1.5), new Circle(2.5));
        System.out.println("Площадь окружностей S=".concat(String.valueOf(getSumArea(circles))));
        addShapes(rectangles, new Rectangle(25.1, 55.0), new Rectangle(5.1, 5.3));
        System.out.println("Площадь прямоугольников S=".concat(String.valueOf(getSumArea(rectangles))));
        addShapes(squares, new Square(25.1), new Square(5.5));
        System.out.println("Площадь квадратов S=".concat(String.valueOf(getSumArea(squares))));
        addShapes(shapes, new Circle(9.9),new Square(5.5),new Rectangle(5.1, 5.3));
        System.out.println("Площадь фигур S=".concat(String.valueOf(getSumArea(shapes))));
    }

}
